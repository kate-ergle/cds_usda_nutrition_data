__author__ = 'jeff'

import random
import math
import numpy as np
import copy

class Optimizer(object):
    def __init__(self, domain, cost_function):

        self.domain = domain
        self.cost_function = cost_function

        #FIXME: Consider making a custom dict for each algo.
        self.records = []


    def iterate(self, n=1):
        results = None
        for i in range(n):
            results = self._loop()
            if results == None:
                return results

        return results

    def _loop(self):
        pass


class RandomOptimize(Optimizer):
    def __init__(self, domain, cost_function):
        super(RandomOptimize, self).__init__(domain, cost_function)

    def _loop(self):
        best = 999999999
        bestr = None
        for i in range(0, 1000):
            # Create a random solution
            r = [float(random.randint(self.domain[i][0], self.domain[i][1]))
                 for i in range(len(self.domain))]

            # Get the cost
            cost = self.cost_function(r)

            # Compare it to the best one so far
            if cost > best:
                  best = cost
                  bestr = r
        return r


class RandomHillClimb(Optimizer):
    def __init__(self, domain, cost_function):
        super(RandomHillClimb, self).__init__(domain, cost_function)

        self.converged = False

        self.solutions = [random.randint(self.domain[i][0], self.domain[i][1])
                          for i in range(len(self.domain))]

        self.iter_count = 0

    def _loop(self):
        # Create list of neighboring solutions
        self.iter_count += 1
        neighbors = []
        for j in range(len(self.domain)):
            # One away in each direction
            if self.solutions[j] > self.domain[j][0]:
                neighbors.append(self.solutions[0:j]+[self.solutions[j]-1]+self.solutions[j+1:])
            if self.solutions[j] < self.domain[j][1]:
                neighbors.append(self.solutions[0:j]+[self.solutions[j]+1]+self.solutions[j+1:])

        # See what the best solution amongst the neighbors is
        current = self.cost_function(self.solutions)
        best = current
        for j in range(len(neighbors)):
            cost = self.cost_function(neighbors[j])

            if cost > best:
                best = cost
                self.solutions = neighbors[j]

        # If there's no improvement, then we've reached the top
        if best == current:
            self.converged = True

        self.records.append((self.solutions, current))
        return self.solutions, current


class GeneticAlgorithm(Optimizer):
    def __init__(self, domain, cost_function, pop_size=50, step=1,
                 mutation_prob=0.2, elite_percent=0.2, elite_prob=1., crossover_blocks=0.125, max_iter=1e10, pool=None):

        super(GeneticAlgorithm, self).__init__(domain, cost_function)

        self.crossover_blocks = crossover_blocks
        self.pop_size = pop_size
        self.step = step
        self.mutation_prob = mutation_prob
        self.elite_percent = elite_percent
        self.max_iter = max_iter
        self.iter_count = 0
        # self.remember_elite = remember_elite
        self.elite_prob = elite_prob
        self.full_results = []
        self.pool = pool


        # How many winners from each generation?
        self.elite_number = int(self.elite_percent*self.pop_size)


        # Build the initial population
        self.current_pop = []
        for i in range(self.pop_size):
            sol_array = [random.randint(domain[i][0], domain[i][1])
                         for i in range(len(domain))]

            self.current_pop.append(sol_array)


    def mutate(self, sol_array):
        # i = random.randint(0, len(self.domain)-1)
        # if random.random() < 0.5 and sol_array[i] > self.domain[i][0]:
        #     return sol_array[0:i]+[sol_array[i]-self.step]+sol_array[i+1:]
        #
        # elif sol_array[i] < self.domain[i][1]:
        #     return sol_array[0:i]+[sol_array[i]+self.step]+sol_array[i+1:]

        mutation = copy.copy(sol_array)
        # random.shuffle(mutation)

        for i, dom_val in zip(range(len(sol_array)), self.domain):
            mutation[i] = random.choice(range(dom_val[1]))

        return mutation

    def crossover(self, r1, r2):
        #FIXME: Add better/more crossover options

        if self.crossover_blocks == 0:
            i = random.randint(0, len(self.domain)-1)
            return r1[0:i]+r2[i:]

        child = []
        while len(child) < len(r1):
            parent = random.choice([r1, r2])
            for i in range(int(len(r1)*self.crossover_blocks)+1):
                if len(child) == len(r1):
                    continue
                else:
                    child.append(parent[len(child)])
            # print('Child length %d'%len(child))

        return child


    def _loop(self):

        # Main loop
        if self.iter_count < self.max_iter:

            if self.pool is None:
                scores = [(self.cost_function(v), v) for v in self.current_pop]

            else:
                scores = self.pool.map(self.cost_function, self.current_pop)
                scores = zip(scores, self.current_pop)

            scores.sort(reverse=True)
            ranked = [v for (s, v) in scores]




            # Start with the pure winners or just create random members if elite_number is 0
            if self.elite_number > .99:
                new_pop = []
                for i in range(self.elite_number):
                    if random.random() < self.elite_prob:
                        new_pop.append(ranked[i])
                    else:
                        new_pop.append(ranked[random.randint(0, len(ranked))])
            else:
                new_pop = []

                for i in range(self.pop_size):
                    sol_array = [random.randint(self.domain[i][0], self.domain[i][1])
                                 for i in range(len(self.domain))]

                    new_pop.append(sol_array)

            # Add mutated and bred forms of the winners
            while len(new_pop) < self.pop_size:
                if random.random() < self.mutation_prob:
                    # Mutation
                    c = random.randint(0, self.elite_number)
                    new_pop.append(self.mutate(ranked[c]))

                else:
                    # Crossover
                    c1 = random.randint(0, self.elite_number)
                    c2 = random.randint(0, self.elite_number)

                    new_pop.append(self.crossover(ranked[c1], ranked[c2]))

            # Print current best score
            self.full_results.append(scores)

            self.iter_count += 1

            self.records.append((np.asarray(scores)[:, 1], scores[0][0]))

            self.current_pop = [individual for individual in new_pop if individual]

            print(np.mean(np.asarray(scores)[:,0]))

            return np.asarray(scores)[:, 1], scores[0][0]

        else:
            return None


class SimulatedAnnealing(Optimizer):
    def __init__(self, domain, cost_function, temperature=100000000000000.0, cool=0.999, step=1):

        super(SimulatedAnnealing, self).__init__(domain, cost_function)

        self.temperature = temperature
        self.cool = cool
        self.step = step

        # Initialize the values randomly
        self.sol_array = [random.randint(domain[i][0], domain[i][1])
                          for i in range(len(domain))]

        self.iter_count = 0

    def _loop(self):
        self.iter_count += 1
        if self.temperature > 0.1:
            # Choose one of the indices
            i = random.randint(0, len(self.domain)-1)

            # Choose a direction to change it
            dir = random.randint(-self.step, self.step)

            # Create a new list with one of the values changed
            sol_arrayb = self.sol_array[:]
            sol_arrayb[i] += dir

            if sol_arrayb[i] < self.domain[i][0]:
                sol_arrayb[i] = self.domain[i][0]

            elif sol_arrayb[i] > self.domain[i][1]:
                sol_arrayb[i] = self.domain[i][1]

            # Calculate the current cost and the new cost
            ea = self.cost_function(self.sol_array)
            eb = self.cost_function(sol_arrayb)
            p = pow(math.e, (-eb-ea)/self.temperature)

            # Is it better, or does it make the probability
            # cutoff?
            if eb > ea or random.random() > p:
                self.sol_array = sol_arrayb

            # Decrease the temperature
            self.temperature *= self.cool

            self.records.append((self.sol_array, eb))

            return self.sol_array, eb

        else:
            return None

