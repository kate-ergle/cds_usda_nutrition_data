__author__ = 'Jeff'

import csv
import pandas as pd
import numpy as np

def to_df():

    entries = {}
    with open('nutrition_data.tsv', 'r') as data_file:
        reader = csv.reader(data_file, delimiter='\t')
        ind = 0
        for row in reader:
            if ind != 0:
                if row[3] not in entries.keys():
					# row[3] -> description
                    entries[row[3]] = {}
                    entries[row[3]]['name'] = row[3]
					# row[6] -> nutrient
					# row[10] -> nutrients.per.gram
					entries[row[3]][row[6]] = row[10]
                else:

                    entries[row[3]][row[6]] = row[10]

            ind += 1


    df = pd.DataFrame(entries.values()).fillna(0)
    df.columns = ['carbs per g', 'calories per g', 'protein per gram', 'fat per g', 'name']



    return df



if __name__ == "__main__":
    df = to_df()

    df.to_csv('nutrition_data_clean.csv')