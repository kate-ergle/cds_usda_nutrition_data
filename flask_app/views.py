__author__ = 'jeff'

from flask import Flask, jsonify
from diet_problem import Diet, Food_DB
from DiscreteOptimizers import GeneticAlgorithm, SimulatedAnnealing, RandomHillClimb

from models import crossdomain
import matplotlib
matplotlib.use("Agg")

import seaborn as sns
sns.set_style('white')
import mpld3

import matplotlib.pyplot as plt
import numpy as np
import time

app = Flask(__name__)

@app.route('/<algorithm>/<run_time>/<calories>/<carbs>/<protein>/<fat>/<favorites>')
@crossdomain(origin='*')
def process(algorithm, run_time, calories, carbs, protein, fat, favorites):
    run_time = int(run_time)
    if run_time > 60:
        run_time = 60
    db = Food_DB()

    key_foods = favorites.split(',')

    diet = Diet(db, 5, calorie_limit=int(calories),
                carb_percent=int(carbs)/100.,
                protein_percent=int(protein)/100.,
                fat_percent=int(fat)/100.,
                key_foods=key_foods)

    if algorithm == 'ga':
        results = run_ga(diet, run_time)

    elif algorithm == 'sa':
        results = run_sa(diet, run_time)

    elif algorithm == 'rhc':
        results = run_rhc(diet, run_time)
    else:
        results = '400'

    plot_array = np.asarray(results[1])*-1

    fig = plt.figure()
    ax = fig.add_subplot(111)
    sns.distplot(plot_array, ax=ax, color='green', bins=10)
    ax.set_title('Cost Distribution')
    ax.set_xlabel('mean: %0.2f, std: %0.2f'%(np.mean(plot_array), np.std(plot_array)))

    response = jsonify(results=results[0], fig=mpld3.fig_to_html(fig))

    return response

def run_ga(diet, run_time):
    ga = GeneticAlgorithm(diet.domain, diet.evaluate, mutation_prob=.1, pop_size=200, elite_prob=0.6,)

    start = time.time()
    while time.time() - start < run_time:
        ga.iterate()

    results = diet.check_results(ga.full_results)[:50]

    return results, map(lambda x: x['score'], results)

def run_sa(diet, run_time):
    sa = SimulatedAnnealing(diet.domain, diet.evaluate)

    start = time.time()
    while time.time() - start < run_time:
        sa.iterate()

    results = diet.check_results(sa.records)[:50]

    return results, map(lambda x: x['score'], results)

def run_rhc(diet, run_time):
    rhc = RandomHillClimb(diet.domain, diet.evaluate)

    start = time.time()
    solutions = []
    while time.time() - start < run_time:
        rhc.iterate()
        if rhc.converged:
            # print('Convergence!')
            solutions.append(rhc.records)
            rhc = RandomHillClimb(diet.domain, diet.evaluate)

    results = diet.check_results([item for sublist in solutions for item in sublist])[:50]

    return results, map(lambda x: x['score'], results)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=False, threaded=False)
