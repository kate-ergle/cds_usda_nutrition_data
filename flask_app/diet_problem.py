__author__ = 'jeff'
import numpy as np
import os

import pandas as pd
import json
from sklearn.metrics import mean_squared_error

file_dir = os.path.dirname(os.path.realpath(__file__))


class Food_DB(object):
    df = pd.read_csv(os.path.join(file_dir, 'nutrition_data_clean.csv'))
    df.set_index(['Index'])
    df = df.convert_objects(convert_numeric=True)

    df = df.dropna()

    # for column_name in df.columns:
    #     df = df[np.isfinite(df[column_name])]
    def __init__(self, serving_sizes=(50, 100, 150, 200)):

        new_entries = []
        for index, row in self.df.iterrows():
            for entry in map(lambda x: list(row) + [x], serving_sizes):
                new_entries.append(entry)

        self.final_df = pd.DataFrame(new_entries, columns=list(self.df.columns) + ['portion g'])


class Diet(object):
    def __init__(self, food_db, num_of_foods, calorie_limit=1090, protein_percent=0.1, fat_percent=0.56, carb_percent=0.34, key_foods=[]):
        self.calorie_limit = calorie_limit
        self.protein_percent = protein_percent
        self.fat_percent = fat_percent
        self.carb_percent = carb_percent
        self.foods = food_db
        self.key_foods = key_foods
        self.num_of_foods = num_of_foods


        self.key_dbs = []
        for food in key_foods:
            self.key_dbs.append(self.foods.final_df[self.foods.final_df['name'].str.contains(food.upper())])
            # print(self.key_dbs[-1])



        self.scoring_list = [self.carb_percent*self.calorie_limit,
                      self.fat_percent*self.calorie_limit,
                      self.protein_percent*self.calorie_limit,
                      self.calorie_limit]

        #FIXME: Build domain to include key_dbs, then get evaluate function to pull from key_dbs
        self.domain = [(0, len(db.index)-1) for db in self.key_dbs]+[(0, len(self.foods.final_df.index)-1)]*(num_of_foods-len(key_foods))



    def evaluate(self, test_array):
        test_array = np.asarray(test_array, dtype=int)
        # print(test_array)
        # print(self.domain)
        # print(test_array[:(len(self.key_dbs))])


        test_dfs = [db.iloc[test_array[ind]] for ind, db in enumerate(self.key_dbs)]

        test_df = pd.DataFrame(test_dfs)#pd.concat(test_dfs, axis=0)
        # print(test_df)
        # print([df for df in self.key_dbs])
        new_df = self.foods.final_df.iloc[test_array[(len(self.key_dbs)):]]


        test_df = pd.concat([test_df, new_df])

        # print(test_df)

        # good_food_reward = np.sum([self.calorie_limit for food in self.key_foods
        #                            if any([food.upper() in name for name in test_df['name']])])

        df1 = pd.DataFrame(test_df.loc[:,'carbs per g']*test_df.loc[:,'portion g'], columns=['carbs'])
        df2 = pd.DataFrame(test_df.loc[:,'calories per g']*test_df.loc[:,'portion g'], columns=['calories'])
        df3 = pd.DataFrame(test_df.loc[:,'protein per g']*test_df.loc[:,'portion g'], columns=['protein'])
        df4 = pd.DataFrame(test_df.loc[:,'fat per g']*test_df.loc[:,'portion g'], columns=['fat'])

        totals_df = pd.concat([df1, df2, df3, df4], join='outer', axis=1)
        # print(totals_df)
        sums_df = totals_df.sum()

        total_calories = sums_df['calories']

        final_df = sums_df[['carbs', 'fat', 'protein']].div(sums_df[['carbs', 'fat', 'protein']].sum()) * self.calorie_limit

        final_df['calories'] = total_calories
        # print(final_df)
        # print(self.scoring_list)
        #
        # print(final_df.values, self.scoring_list)

        mse = mean_squared_error(final_df.values, self.scoring_list)


        return np.asarray([-mse])#+2*good_food_reward])

    def check_results(self, results, web_app=False):
        # print(len(results))
        # print(results[0])

        seen_combos = []
        keep_combos = []
        for iter_results in results:
            # print(iter_results)
            for result in iter_results[:10]:
                try:
                    score, test_array = result
                except ValueError:
                    test_array, score = iter_results

                if repr(np.unique(test_array)) not in seen_combos:
                    seen_combos.append(repr(np.unique(test_array)))
                    keep_combos.append({})
                    test_dfs = [db.iloc[test_array[ind]] for ind, db in enumerate(self.key_dbs)]

                    test_df = pd.DataFrame(test_dfs)#pd.concat(test_dfs, axis=0)
                    # print(test_df)
                    # print([df for df in self.key_dbs])
                    new_df = self.foods.final_df.iloc[test_array[(len(self.key_dbs)):]]


                    test_df = pd.concat([test_df, new_df])


                    # print('\n\n')
                    # print(test_df[['name', 'portion g']])

                    i = 0
                    for index, item in test_df[['name', 'portion g']].iterrows():
                        item = dict(item)
                        keep_combos[-1]['item%d'%i] = item['name'] + ' : ' + str(item['portion g'])+'g'
                        i += 1

                    df1 = pd.DataFrame(test_df.loc[:,'carbs per g']*test_df.loc[:,'portion g'], columns=['carbs'])
                    df2 = pd.DataFrame(test_df.loc[:,'calories per g']*test_df.loc[:,'portion g'], columns=['calories'])
                    df3 = pd.DataFrame(test_df.loc[:,'protein per g']*test_df.loc[:,'portion g'], columns=['protein'])
                    df4 = pd.DataFrame(test_df.loc[:,'fat per g']*test_df.loc[:,'portion g'], columns=['fat'])

                    totals_df = pd.concat([df1, df2, df3, df4], join='outer', axis=1)

                    sums_df = totals_df.sum()

                    total_calories = sums_df['calories']

                    final_df = sums_df[['carbs', 'fat', 'protein']].div(sums_df[['carbs', 'fat', 'protein']].sum())
                    final_df['calories'] = total_calories

                    keep_combos[-1]['calories'] = final_df['calories']
                    keep_combos[-1]['carbs'] = final_df['carbs']
                    keep_combos[-1]['fat'] = final_df['fat']
                    keep_combos[-1]['protein'] = final_df['protein']
                    keep_combos[-1]['score'] = score[0]

                    # print(keep_combos[-1])
                    # print(final_df)

        return sorted(keep_combos, key=lambda x: x['score'], reverse=True)



if __name__ == "__main__":
    pass
    # db = Food_DB()
    #
    # test = Diet(db, 5, key_foods=['chicken', 'peanuts'])
    #
    # ga = GeneticAlgorithm(test.domain, test.evaluate, mutation_prob=.4, pop_size=100, elite_percent=0.6)
    #
    # ga.iterate(50)
    #
    # test.check_results(ga.full_results)